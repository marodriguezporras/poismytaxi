# PoisMyTaxi app

MyTaxi Android Coding Challenge.

App that allows the list the vehicles within given bounds.
The vehicles are shown both in list and in map.

The architecture of the app is based on [Uncle Bob's clean architecture approach](https://8thlight.com/blog/uncle-bob/2012/08/13/the-clean-architecture.html) 
and [Fernando Cejas Android-CleanArchitecture](https://github.com/android10/Android-CleanArchitecture-Kotlin)
sample app.

The justification behind this approach is that it makes it really easy to guess what the project is 
about only by looking its directory structure, also it facilitates the adding or removal of features 
and finally it enables the modification of one module without interfering with the others.

## Important notes

 * The challenge was developed 100% in Kotlin and built with Android Studio 3.3.1
 * When running the app, do it on a physical device not on the emulator to ensure that the Google Play
 Services are properly installed 
 
 
## Usage and features

The app allows for the visualization of vehicles on the map. When the user zooms and pans on the map
the vehicles are refreshed to show the ones within the new bounds. 
 * When visualizing the map, at the bottom there is button which displays a drawer containing a list 
 of the vehicles currently displayed on the map.
 * When visualizing the map, you can click on any marker to center the map around that vehicle.
 * When visualizing the list, you can choose from a list of favourite cities (currently only Hamburg)
 to center the map on that city and list the vehicles withing the city's bounds.
 * When visualizing the list, you can click on any item and it will show you the location of that 
 vehicle on the map
 

## Tools and libraries

The project uses the following external libraries:

 * **dagger**: For dependency injection
 * **retrofit**: For network calls
 * **slidinguppanel**: For the drawer widget
 * **rx java**: For in-between module communication and thread handling
 
## Modules

The app consists mainly of 3 modules: app, domain and data. The modules communicate with each other
relaying on abstractions or interfaces (rather than concretions) so it would fairly simple to change
one without affecting the other.

### App

Also called presentation, this is where the logic related with views is located. It uses the Model 
View Presenter pattern.
 
### Domain

This module doesn't have any android dependencies, this is because here only is the core business 
logic without any dependency to any framework.

The core business logic is implemented in the form of use cases.

The domain module also serves as the place to define the core entities (usually POJOs or in this case
Kotlin's data classes) and the interface for the data module to implement.

Overall the app uses 2 different threads. One for the UI updates (main thread) and other one for 
everything else (network requests, mappings, and so on), the use case base classes is where 
all of this is specified.
  
### Data

All data needed for the application comes from this module which uses the 
[Repository Pattern](https://martinfowler.com/eaaCatalog/repository.html). 

The packaging/organization of the code was intended to be as feature driven as possible.

## Testing

Tests were written in all three modules. 
 * **Unit tests**: All three modules contain unit tests using junit and mockito.
 * **Integration tests**: This tests were made on the app module only and were done using espresso.
 These are UI tests which test both the app and domain module while mocking the data module.
 
Note that the app module contains both Unit (presenter) and Integration (Activity and other 
android components) tests.