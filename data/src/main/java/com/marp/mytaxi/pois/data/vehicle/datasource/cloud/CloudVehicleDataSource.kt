package com.marp.mytaxi.pois.data.vehicle.datasource.cloud

import com.marp.mytaxi.pois.data.vehicle.datasource.VehicleDataSource
import com.marp.mytaxi.pois.domain.map.models.Coordinate
import com.marp.mytaxi.pois.domain.vehicle.models.Vehicle
import io.reactivex.Observable

class CloudVehicleDataSource(
        private val restApi: VehicleApiRest) : VehicleDataSource {

    override fun vehiclesListFromCoordinates(c1: Coordinate, c2: Coordinate): Observable<List<Vehicle>> {
        return this.restApi.vehiclesListFromCoordinates(c1, c2)
    }
}
