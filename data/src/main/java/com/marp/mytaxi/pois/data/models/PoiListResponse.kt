package com.marp.mytaxi.pois.data.models

data class PoiListResponse<T>(val poiList: List<T>)