package com.marp.mytaxi.pois.data.vehicle.datasource

import com.marp.mytaxi.pois.domain.map.models.Coordinate
import com.marp.mytaxi.pois.domain.vehicle.models.Vehicle
import io.reactivex.Observable

interface VehicleDataSource {
    fun vehiclesListFromCoordinates(c1: Coordinate, c2: Coordinate): Observable<List<Vehicle>>
}

