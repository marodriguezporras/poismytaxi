package com.marp.mytaxi.pois.data.vehicle.datasource.cloud

import com.marp.mytaxi.pois.data.net.RestClient
import com.marp.mytaxi.pois.domain.map.models.Coordinate
import com.marp.mytaxi.pois.domain.vehicle.models.Vehicle
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class VehicleApiRest @Inject
constructor(private val restClient: RestClient) {
    fun vehiclesListFromCoordinates(c1: Coordinate, c2: Coordinate): Observable<List<Vehicle>> {
        return restClient
                .endpoints()
                .vehiclesListFromCoordinates(c1.lat, c1.lon, c2.lat, c2.lon)
                .map { response -> response.poiList }
    }
}
