package com.marp.mytaxi.pois.data.net.exception

import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

class RetrofitException internal constructor(
        message: String?,
         /** The request URL which produced the error.  */
         val url: String?,
         /** Response object containing status code, headers, body, etc.  */
         val response: Response<*>?,
         /** The event kind which triggered this error.  */
         val kind: Kind, exception: Throwable) : RuntimeException(message, exception) {

    /** Identifies the event kind which triggered a [RetrofitException].  */
    enum class Kind {
        /** An [IOException] occurred while communicating to the server.  */
        NETWORK,
        /** A non-200 HTTP status code was received from the server.  */
        HTTP,
        /**
         * An internal error occurred while attempting to execute a request. It is best practice to
         * re-throw this exception so your application crashes.
         */
        UNEXPECTED
    }

    companion object {
        fun httpError(httpException: HttpException): RetrofitException {
            val response = httpException.response()

            var message: String
            try {
                message = httpException.response().errorBody()!!.string()
            } catch (e: IOException) {
                e.printStackTrace()
                message = response.code().toString() + " " + response.message()
            }

            return RetrofitException(message, response.raw().request().url().toString(), httpException.response(), Kind.HTTP, httpException)
        }

        fun networkError(exception: IOException): RetrofitException {
            return RetrofitException(exception.message, null, null, Kind.NETWORK, exception)
        }

        fun unexpectedError(exception: Throwable): RetrofitException {
            return RetrofitException(exception.message, null, null, Kind.UNEXPECTED, exception)
        }
    }
}

