package com.marp.mytaxi.pois.data.vehicle

import com.marp.mytaxi.pois.data.vehicle.datasource.VehicleDataSourceFactory
import com.marp.mytaxi.pois.domain.map.models.Coordinate
import com.marp.mytaxi.pois.domain.vehicle.VehicleRepository
import com.marp.mytaxi.pois.domain.vehicle.models.Vehicle
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class VehicleDataRepository @Inject
constructor(private val vehicleDataSourceFactory: VehicleDataSourceFactory) : VehicleRepository {

    override fun vehicleListFromCoordinates(c1: Coordinate, c2: Coordinate): Observable<List<Vehicle>> {
        val vehicleDataSource = this.vehicleDataSourceFactory.createVehicleCloudDataSource()
        return vehicleDataSource.vehiclesListFromCoordinates(c1, c2)
    }
}
