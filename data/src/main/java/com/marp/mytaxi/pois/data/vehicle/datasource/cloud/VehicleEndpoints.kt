package com.marp.mytaxi.pois.data.vehicle.datasource.cloud

import com.marp.mytaxi.pois.data.models.PoiListResponse
import com.marp.mytaxi.pois.domain.vehicle.models.Vehicle
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface VehicleEndpoints {
    companion object {
        const val API_URL_GET_VEHICLE_LIST = "?"
        const val API_BASE_URL = "https://fake-poi-api.mytaxi.com/"
    }

    @GET(API_URL_GET_VEHICLE_LIST)
    fun vehiclesListFromCoordinates(@Query("p1Lat") p1Lat: Double, @Query("p1Lon") p1Lon: Double,
                                    @Query("p2Lat") p2Lat: Double, @Query("p2Lon") p2Lon: Double):
            Observable<PoiListResponse<Vehicle>>

}
