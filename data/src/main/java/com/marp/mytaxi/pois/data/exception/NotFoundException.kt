package com.marp.mytaxi.pois.data.exception

class NotFoundException : Exception()