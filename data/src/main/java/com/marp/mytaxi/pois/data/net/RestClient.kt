package com.marp.mytaxi.pois.data.net

import com.marp.mytaxi.pois.data.vehicle.datasource.cloud.VehicleEndpoints
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RestClient @Inject
constructor(private val baseUrl: String) {

    private lateinit var apiObject: VehicleEndpoints

    init {
        createApiObject()
    }

    private fun createApiObject() {
        val httpClient = OkHttpClient.Builder()
        httpClient.connectTimeout(50, TimeUnit.SECONDS)
        httpClient.writeTimeout(50, TimeUnit.SECONDS)
        httpClient.readTimeout(50, TimeUnit.SECONDS)

        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(httpLoggingInterceptor)

        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxErrorHandling.create())
                .client(httpClient.build())
                .build()

        apiObject = retrofit.create(VehicleEndpoints::class.java)
    }

    internal fun endpoints(): VehicleEndpoints {
        return apiObject
    }

}