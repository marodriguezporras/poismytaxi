package com.marp.mytaxi.pois.data.vehicle.datasource

import com.marp.mytaxi.pois.data.vehicle.datasource.cloud.CloudVehicleDataSource
import com.marp.mytaxi.pois.data.vehicle.datasource.cloud.VehicleApiRest
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class VehicleDataSourceFactory @Inject
constructor(private var restApi: VehicleApiRest){

    fun createVehicleCloudDataSource(): VehicleDataSource {
        return CloudVehicleDataSource(restApi)
    }
}
