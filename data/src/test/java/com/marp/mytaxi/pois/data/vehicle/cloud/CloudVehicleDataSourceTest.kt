package com.marp.mytaxi.pois.data.vehicle.cloud

import com.marp.mytaxi.pois.data.vehicle.datasource.cloud.CloudVehicleDataSource
import com.marp.mytaxi.pois.data.vehicle.datasource.cloud.VehicleApiRest
import com.marp.mytaxi.pois.domain.map.models.Coordinate
import com.marp.mytaxi.pois.domain.vehicle.models.FleetType
import com.marp.mytaxi.pois.domain.vehicle.models.Vehicle
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.util.ArrayList

@RunWith(MockitoJUnitRunner::class)
class CloudVehicleDataSourceTest{
    private lateinit var cloudVehicleDataSource: CloudVehicleDataSource
    private val mockRestApi: VehicleApiRest = mock()

    @Before
    fun setUp() {
        cloudVehicleDataSource = CloudVehicleDataSource(mockRestApi)
    }

    @Test
    fun givenRestApiResponse_whenListFromCoordinates_thenRestApiIsCalled() {
        // GIVEN
        val vehicleList = ArrayList<Vehicle>()
        vehicleList.add(Vehicle(
                "id", Coordinate(0.0, 0.0), FleetType.POOLING
        ))
        val c1 = Coordinate(0.0, 0.0)
        val c2 = Coordinate(0.0, 0.0 )
        given(mockRestApi.vehiclesListFromCoordinates(c1, c2)).willReturn(Observable.just(vehicleList))

        // WHEN
        cloudVehicleDataSource.vehiclesListFromCoordinates(c1, c2)

        // THEN
        verify(mockRestApi).vehiclesListFromCoordinates(c1, c2)
    }
}