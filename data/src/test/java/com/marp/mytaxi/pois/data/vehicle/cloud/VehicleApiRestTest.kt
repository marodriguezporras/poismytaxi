package com.marp.mytaxi.pois.data.vehicle.cloud

import com.marp.mytaxi.pois.data.models.PoiListResponse
import com.marp.mytaxi.pois.data.net.RestClient
import com.marp.mytaxi.pois.data.vehicle.VehicleDataRepository
import com.marp.mytaxi.pois.data.vehicle.datasource.VehicleDataSource
import com.marp.mytaxi.pois.data.vehicle.datasource.VehicleDataSourceFactory
import com.marp.mytaxi.pois.data.vehicle.datasource.cloud.CloudVehicleDataSource
import com.marp.mytaxi.pois.data.vehicle.datasource.cloud.VehicleApiRest
import com.marp.mytaxi.pois.data.vehicle.datasource.cloud.VehicleEndpoints
import com.marp.mytaxi.pois.domain.map.models.Coordinate
import com.marp.mytaxi.pois.domain.vehicle.models.FleetType
import com.marp.mytaxi.pois.domain.vehicle.models.Vehicle
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.util.ArrayList

@RunWith(MockitoJUnitRunner::class)
class VehicleApiRestTest{

    private lateinit var vehicleApiRest: VehicleApiRest
    private val mockRestClient: RestClient = mock()
    private val mockEndpoints: VehicleEndpoints = mock()

    @Before
    fun setUp() {
        given(mockRestClient.endpoints()).willReturn(mockEndpoints)
        vehicleApiRest = VehicleApiRest(mockRestClient)
    }

    @Test
    fun givenEndpointsResponse_whenListFromCoordinates_thenEndpointsIsCalled() {
        // GIVEN
        val vehicleList = ArrayList<Vehicle>()
        vehicleList.add(Vehicle(
                "id", Coordinate(0.0, 0.0), FleetType.POOLING
        ))
        val c1 = Coordinate(0.0, 0.0)
        val c2 = Coordinate(0.0, 0.0 )
        val poiResponse = PoiListResponse(vehicleList)
        given(mockEndpoints.vehiclesListFromCoordinates(c1.lat, c1.lon, c2.lat, c2.lon))
                .willReturn(Observable.just(poiResponse))

        // WHEN
        vehicleApiRest.vehiclesListFromCoordinates(c1, c2)

        // THEN
        verify(mockRestClient).endpoints()
        verify(mockEndpoints).vehiclesListFromCoordinates(c1.lat, c1.lon, c2.lat, c2.lon)
    }
}