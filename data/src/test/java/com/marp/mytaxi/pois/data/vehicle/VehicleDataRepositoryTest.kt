package com.marp.mytaxi.pois.data.vehicle

import com.marp.mytaxi.pois.data.vehicle.datasource.VehicleDataSource
import com.marp.mytaxi.pois.data.vehicle.datasource.VehicleDataSourceFactory
import com.marp.mytaxi.pois.domain.map.models.Coordinate
import com.marp.mytaxi.pois.domain.vehicle.models.FleetType
import com.marp.mytaxi.pois.domain.vehicle.models.Vehicle
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.util.ArrayList

@RunWith(MockitoJUnitRunner::class)
class VehicleDataRepositoryTest {

    private lateinit var vehicleDataRepository: VehicleDataRepository
    private val mockVehicleDataSourceFactory: VehicleDataSourceFactory = mock()
    private val mockVehicleDataSource: VehicleDataSource = mock()


    @Before
    fun setUp() {
        given(mockVehicleDataSourceFactory.createVehicleCloudDataSource()).willReturn(mockVehicleDataSource)
        vehicleDataRepository = VehicleDataRepository(mockVehicleDataSourceFactory)
    }

    @Test
    fun givenDataSourceResponse_whenListFromCoordinates_thenDataSourceIsCalled() {
        // GIVEN
        val vehicleList = ArrayList<Vehicle>()
        vehicleList.add(Vehicle(
                "id", Coordinate(0.0, 0.0), FleetType.POOLING
        ))
        val c1 = Coordinate(0.0, 0.0)
        val c2 = Coordinate(0.0, 0.0 )
        given(mockVehicleDataSource.vehiclesListFromCoordinates(c1, c2)).willReturn(Observable.just(vehicleList))

        // WHEN
        vehicleDataRepository.vehicleListFromCoordinates(c1, c2)

        // THEN
        verify(mockVehicleDataSource).vehiclesListFromCoordinates(c1, c2)
    }
}
