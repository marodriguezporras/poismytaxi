package com.marp.mytaxi.pois.domain.vehicle.list

import com.marp.mytaxi.pois.domain.map.models.Coordinate
import com.marp.mytaxi.pois.domain.vehicle.VehicleRepository
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Scheduler
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class VehicleListUseCaseTest{
    private val C1 = Coordinate(0.0, 0.0)
    private val C2 = Coordinate(0.0, 0.0)

    private lateinit var vehicleListUseCase: VehicleListUseCase
    private val mockWorkerExecutor: Scheduler = mock()
    private val mockPostExecutionThread: Scheduler = mock()
    private val mockVehicleRepository: VehicleRepository = mock()

    @Before
    fun setUp() {
        vehicleListUseCase = VehicleListUseCase(mockWorkerExecutor, mockPostExecutionThread,
                mockVehicleRepository)
    }

    @Test
    fun whenBuildObservable_thenRepositoryIsCalled() {
        // WHEN
        vehicleListUseCase.buildUseCaseObservable(VehicleListUseCase.Params.forVehicleList(C1, C2))

        // THEN
        verify(mockVehicleRepository).vehicleListFromCoordinates(C1, C2)
        verifyNoMoreInteractions(mockVehicleRepository)
        verifyZeroInteractions(mockPostExecutionThread)
        verifyZeroInteractions(mockWorkerExecutor)
    }
}