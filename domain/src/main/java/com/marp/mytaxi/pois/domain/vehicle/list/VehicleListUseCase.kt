package com.marp.mytaxi.pois.domain.vehicle.list

import com.marp.mytaxi.pois.domain.base.UseCase
import com.marp.mytaxi.pois.domain.map.models.Coordinate
import com.marp.mytaxi.pois.domain.vehicle.VehicleRepository
import com.marp.mytaxi.pois.domain.vehicle.models.Vehicle
import io.reactivex.Observable
import io.reactivex.Scheduler
import javax.inject.Inject
import javax.inject.Named

class VehicleListUseCase @Inject
constructor(@Named("worker_thread") workerExecutor: Scheduler,
            @Named("ui_thread") postExecutionThread: Scheduler,
            private val vehicleRepository: VehicleRepository) :
                UseCase<List<Vehicle>,
                        VehicleListUseCase.Params>(workerExecutor, postExecutionThread) {

    override fun buildUseCaseObservable(params: Params): Observable<List<Vehicle>> {
        return this.vehicleRepository.vehicleListFromCoordinates(params.c1, params.c2)
    }

    data class Params (val c1: Coordinate, val c2: Coordinate) {
        companion object {
            fun forVehicleList(c1: Coordinate, c2: Coordinate): Params {
                return Params(c1, c2)
            }
        }
    }
}
