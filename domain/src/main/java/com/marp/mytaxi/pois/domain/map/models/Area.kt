package com.marp.mytaxi.pois.domain.map.models

data class Area(val name: String, val northwest: Coordinate, val southeast: Coordinate){
    constructor(northwest: Coordinate, southeast: Coordinate) : this("", northwest, southeast)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Area

        if (northwest != other.northwest) return false
        if (southeast != other.southeast) return false

        return true
    }

    override fun hashCode(): Int {
        var result = northwest.hashCode()
        result = 31 * result + southeast.hashCode()
        return result
    }

}

