package com.marp.mytaxi.pois.domain.vehicle.models

import com.marp.mytaxi.pois.domain.map.models.Coordinate

data class Vehicle(
        val id: String,
        val coordinate: Coordinate,
        val fleetType: FleetType,
        val heading: Double = 0.0)

