package com.marp.mytaxi.pois.domain.base

import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver

abstract class UseCase<T, Params>(
        private val workerExecutor: Scheduler,
        private val postExecutionThread: Scheduler) {
    private val disposables: CompositeDisposable = CompositeDisposable()

    abstract fun buildUseCaseObservable(params: Params): Observable<T>

    fun execute(observer: DisposableObserver<T>, params: Params) {
        val observable = this.buildUseCaseObservable(params)
                .subscribeOn(workerExecutor)
                .observeOn(postExecutionThread)
        disposables.add(observable.subscribeWith(observer))

    }

    fun unsubscribe() {
        if (!disposables.isDisposed) {
            disposables.dispose()
        }
    }
}