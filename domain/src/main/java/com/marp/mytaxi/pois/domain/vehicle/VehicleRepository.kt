package com.marp.mytaxi.pois.domain.vehicle

import com.marp.mytaxi.pois.domain.map.models.Coordinate
import com.marp.mytaxi.pois.domain.vehicle.models.Vehicle
import io.reactivex.Observable

interface VehicleRepository {
    fun vehicleListFromCoordinates(c1: Coordinate, c2: Coordinate): Observable<List<Vehicle>>
}