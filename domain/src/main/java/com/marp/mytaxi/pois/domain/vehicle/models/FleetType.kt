package com.marp.mytaxi.pois.domain.vehicle.models

enum class FleetType(val color: String) {
    TAXI("#DF0101"),
    POOLING("#0404B4")
}
