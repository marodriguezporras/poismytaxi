package com.marp.mytaxi.pois.domain.map.models

import com.google.gson.annotations.SerializedName

data class Coordinate(
        @SerializedName("latitude")
        val lat: Double,
        @SerializedName("longitude")
        val lon: Double){

    override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Coordinate

            if (lat != other.lat) return false
            if (lon != other.lon) return false

            return true
    }

    override fun hashCode(): Int {
            var result = lat.hashCode()
            result = 31 * result + lon.hashCode()
            return result
}
}
