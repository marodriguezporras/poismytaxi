package com.marp.mytaxi.pois.utils

import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import com.marp.mytaxi.pois.PoisApplication
import com.marp.mytaxi.pois.injector.components.AppComponent
import com.marp.mytaxi.pois.injector.modules.AppModule
import it.cosenonjaviste.daggermock.DaggerMock


fun espressoDaggerMockRule() = DaggerMock.rule<AppComponent>(AppModule(app)) {
    set { component -> app.setComponent(AppComponent::class.java, component) }
}


val app: PoisApplication get() = getInstrumentation().targetContext.applicationContext as PoisApplication
