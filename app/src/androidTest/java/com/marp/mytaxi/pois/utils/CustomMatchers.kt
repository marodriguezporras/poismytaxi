package com.marp.mytaxi.pois.utils

import android.annotation.SuppressLint
import android.view.View
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.matcher.BoundedMatcher
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher

class CustomMatchers{
    companion object {
        fun withIndex(matcher: Matcher<View>, index: Int): Matcher<View> {
            return object : TypeSafeMatcher<View>() {
                internal var currentIndex: Int = 0
                internal var viewObjHash: Int = 0

                @SuppressLint("DefaultLocale")
                override fun describeTo(description: Description) {
                    description.appendText(String.format("with index: %d ", index))
                    matcher.describeTo(description)
                }

                public override fun matchesSafely(view: View): Boolean {
                    if (matcher.matches(view) && currentIndex++ == index) {
                        viewObjHash = view.hashCode()
                    }
                    return view.hashCode() == viewObjHash
                }
            }
        }

        fun withTextIgnoreCase(@StringRes resourceId: Int): Matcher<View> {
            return object : BoundedMatcher<View, TextView>(TextView::class.java) {
                override fun describeTo(description: Description) {
                    description.appendText("with string from resource id: ").appendValue(resourceId)
                }

                public override fun matchesSafely(textView: TextView): Boolean {
                    val expectedText = textView.resources.getString(resourceId)
                    val actualText = textView.text
                    return null != actualText && expectedText.toLowerCase() == actualText.toString().toLowerCase()
                }
            }
        }

        fun withListSize(size: Int): Matcher<View> {
            return object : TypeSafeMatcher<View>() {
                public override fun matchesSafely(view: View): Boolean {
                    return (view as RecyclerView).adapter!!.itemCount == size
                }

                override fun describeTo(description: Description) {
                    description.appendText("ListView should have $size items")
                }
            }
        }
    }
}