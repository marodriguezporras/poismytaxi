package com.marp.mytaxi.pois.vehicle

import android.content.Intent
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.marp.mytaxi.pois.R
import com.marp.mytaxi.pois.domain.map.models.Coordinate
import com.marp.mytaxi.pois.domain.vehicle.VehicleRepository
import com.marp.mytaxi.pois.domain.vehicle.models.FleetType
import com.marp.mytaxi.pois.domain.vehicle.models.Vehicle
import com.marp.mytaxi.pois.utils.CustomMatchers.Companion.withListSize
import com.marp.mytaxi.pois.utils.espressoDaggerMockRule
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.amshove.kluent.any


import org.junit.Rule

import org.amshove.kluent.shouldBeInstanceOf
import org.amshove.kluent.shouldNotBeNull
import org.hamcrest.CoreMatchers.startsWith
import org.hamcrest.Matchers.hasToString
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
@LargeTest
class VehicleListActivityTest{
    @get:Rule
    val daggerRule = espressoDaggerMockRule()
    @get:Rule
    var activityRule = ActivityTestRule(VehicleListActivity::class.java, false, false)
    val vehicleRepository: VehicleRepository = mock()

    @Before
    fun before() {

    }

    @Test
    fun whenActivityStarted_thenContainsFragments() {
        whenever(vehicleRepository.vehicleListFromCoordinates(any(), any()))
                .thenReturn(Observable.just(arrayListOf()))

        // WHEN
        activityRule.launchActivity(Intent())

        // THEN
        activityRule.activity.supportFragmentManager.findFragmentById(R.id.fragment_container).let { fragment ->
            fragment.shouldNotBeNull()
            fragment shouldBeInstanceOf VehicleListFragment::class.java
        }
    }

    @Test
    fun givenOneVehicle_whenLoadVehicles_thenVehicleInListIsShown() {
        // GIVEN
        val vehicle = Vehicle("initializeVehicle", Coordinate(0.0, 0.0), FleetType.POOLING)
        whenever(vehicleRepository.vehicleListFromCoordinates(any(), any()))
                .thenReturn(Observable.just(arrayListOf(vehicle)))
        activityRule.launchActivity(Intent())

        // WHEN
        onView(withId(R.id.tv_title)).check(matches(isDisplayed())).perform(click())

        // THEN
        onView(withText(vehicle.id)).check(matches(isDisplayed()))
        onView(withText(vehicle.fleetType.name)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_vehicles)).check(matches(withListSize(1)))
    }

    @Test
    fun givenOneVehicle_whenFavCitySelected_thenVehicleInListIsReloaded() {
        // GIVEN
        val vehicle1 = Vehicle("initializeVehicle", Coordinate(0.0, 0.0), FleetType.POOLING)
        val vehicle2 = Vehicle("cameraIdleVehicle", Coordinate(0.0, 0.0), FleetType.POOLING)
        val vehicle3 = Vehicle("favCityVehicle", Coordinate(0.0, 0.0), FleetType.POOLING)
        whenever(vehicleRepository.vehicleListFromCoordinates(any(), any()))
                .thenReturn(Observable.just(arrayListOf(vehicle1)))
                .thenReturn(Observable.just(arrayListOf(vehicle2)))
                .thenReturn(Observable.just(arrayListOf(vehicle3)))
        activityRule.launchActivity(Intent())

        // WHEN
        onView(withId(R.id.tv_title)).check(matches(isDisplayed())).perform(click())
        onView(withText(vehicle2.id)).check(matches(isDisplayed()))
        onView(withId(R.id.fav_cities)).check(matches(isDisplayed())).perform(click())
        onData(hasToString(startsWith("Hamburg"))).perform(click())

        // THEN
        onView(withText(vehicle3.id)).check(matches(isDisplayed()))
        onView(withText(vehicle3.fleetType.name)).check(matches(isDisplayed()))
        onView(withId(R.id.rv_vehicles)).check(matches(withListSize(1)))
    }
}