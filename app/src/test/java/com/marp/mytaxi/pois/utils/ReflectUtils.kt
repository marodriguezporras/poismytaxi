package com.marp.mytaxi.pois.utils


fun <T> Any.getPrivate(fieldName: String) = with(this) {
    val privateStringField = this::class.java.getDeclaredField(fieldName)
    privateStringField.isAccessible = true
    privateStringField.get(this) as T
}

fun <T> Any.setPrivate(fieldName: String, value: T) {
    this.let { objInstance ->
        objInstance::class.java.getDeclaredField(fieldName).apply {
            isAccessible = true
            set(objInstance, value)
        }
    }
}
