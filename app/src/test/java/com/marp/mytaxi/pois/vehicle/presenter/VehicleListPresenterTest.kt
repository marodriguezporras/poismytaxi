package com.marp.mytaxi.pois.vehicle.presenter

import com.marp.mytaxi.pois.domain.map.models.Coordinate
import com.marp.mytaxi.pois.domain.vehicle.list.VehicleListUseCase
import com.marp.mytaxi.pois.domain.vehicle.models.FleetType
import com.marp.mytaxi.pois.domain.vehicle.models.Vehicle
import com.marp.mytaxi.pois.utils.setPrivate
import com.nhaarman.mockito_kotlin.*
import io.reactivex.observers.DisposableObserver
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import java.util.ArrayList


@RunWith(MockitoJUnitRunner::class)
class VehicleListPresenterTest{
    private lateinit var vehicleListPresenter: VehicleListPresenter
    private val interactorMock: VehicleListInteractor = mock()
    private val vehicleListUseCaseMock: VehicleListUseCase = mock()
    private val viewMock:VehicleListPresenter.View = mock()

    @Before
    fun setUp() {
        vehicleListPresenter = VehicleListPresenter(interactorMock)
        vehicleListPresenter.attachView(viewMock)
    }

    @After
    fun validate() {
        validateMockitoUsage()
    }

    @Test
    fun givenUseCase_whenDestroy_thenUseCaseUnsubscribe(){
        // GIVEN
        given(interactorMock.vehicleListUseCase).willReturn(vehicleListUseCaseMock)

        // WHEN
        vehicleListPresenter.destroy()

        // THEN
        verify(vehicleListUseCaseMock).unsubscribe()
        verifyNoMoreInteractions(vehicleListUseCaseMock)
    }

    @Test
    fun givenVehicle_whenVehicleClicked_thenShowMarkerInfo(){
        // GIVEN
        val vehicle = Vehicle("id", Coordinate(0.0, 0.0), FleetType.POOLING)

        // WHEN
        vehicleListPresenter.onVehicleClicked(vehicle)


        // THEN
        inOrder(viewMock){
            verify(viewMock).focusMapOn(vehicle.coordinate, 13f)
            verify(viewMock).showMarkerVehicleInfo(vehicle)
            verify(viewMock).closePanel()
        }
        verifyNoMoreInteractions(viewMock)
    }

    @Test
    @Suppress("UNCHECKED_CAST")
    fun givenVehicleList_whenInitialize_thenLoadVehiclesAndInitView(){
        // GIVEN
        given(interactorMock.vehicleListUseCase).willReturn(vehicleListUseCaseMock)
        given(viewMock.getString(any())).willReturn("Select one")
        val vehicleList = ArrayList<Vehicle>()
        vehicleList.add(Vehicle(
                "id", Coordinate(0.0, 0.0), FleetType.POOLING
        ))
        whenever(vehicleListUseCaseMock.execute(any(), any())).then { invocation ->
            val observer = invocation.arguments[0] as DisposableObserver<List<Vehicle>>
            observer.onNext(vehicleList)
            observer.onComplete()
        }

        // WHEN
        vehicleListPresenter.initialize()

        // THEN
        inOrder(viewMock) {
            verify(viewMock).hideRetry()
            verify(viewMock).showLoading()
            verify(viewMock).renderVehiclesOnList(vehicleList)
            verify(viewMock).renderVehiclesOnMap(vehicleList)
            verify(viewMock).hideLoading()
            verify(viewMock).getString(any())
            verify(viewMock).initFavouriteCities(arrayListOf("Select one", "Hamburg"))
        }
        verifyNoMoreInteractions(viewMock)
    }

    @Test
    fun givenVehiclesReloadAllowed_whenCameraIdle_thenLoadVehicles(){
        // GIVEN
        vehicleListPresenter.setPrivate("vehiclesReloadAllowed", true)
        given(interactorMock.vehicleListUseCase).willReturn(vehicleListUseCaseMock)
        given(viewMock.getVisibleArea()).willReturn(VehicleListPresenter.HAMBURG)
        val vehicleList = ArrayList<Vehicle>()
        vehicleList.add(Vehicle(
                "id", Coordinate(0.0, 0.0), FleetType.POOLING
        ))
        whenever(vehicleListUseCaseMock.execute(any(), any())).then { invocation ->
            val observer = invocation.arguments[0] as DisposableObserver<List<Vehicle>>
            observer.onNext(vehicleList)
            observer.onComplete()
        }

        // WHEN
        vehicleListPresenter.onCameraIdle()

        // THEN
        inOrder(viewMock) {
            verify(viewMock).getVisibleArea()
            verify(viewMock).hideRetry()
            verify(viewMock).showLoading()
            verify(viewMock).renderVehiclesOnList(vehicleList)
            verify(viewMock).renderVehiclesOnMap(vehicleList)
            verify(viewMock).hideLoading()
        }
        verifyNoMoreInteractions(viewMock)
    }

    @Test
    fun givenNoVehiclesReloadAllowed_whenCameraIdle_thenNoActions(){
        // GIVEN
        vehicleListPresenter.setPrivate("vehiclesReloadAllowed", false)

        // WHEN
        vehicleListPresenter.onCameraIdle()

        // THEN
        verifyZeroInteractions(viewMock)
        verifyZeroInteractions(interactorMock)
    }

    @Test
    fun whenMapReady_thenFocusOnHamburg(){
        // WHEN
        vehicleListPresenter.onMapReady()

        // THEN
        verify(viewMock).focusMapOn(VehicleListPresenter.HAMBURG)
        verifyZeroInteractions(viewMock)
    }

    @Test
    fun givenFavCities_whenFavCitySelected_thenLoadVehicles(){
        // GIVEN
        vehicleListPresenter.setPrivate("favouriteCities", arrayListOf(VehicleListPresenter.HAMBURG))
        given(interactorMock.vehicleListUseCase).willReturn(vehicleListUseCaseMock)
        val vehicleList = ArrayList<Vehicle>()
        vehicleList.add(Vehicle(
                "id", Coordinate(0.0, 0.0), FleetType.POOLING
        ))
        whenever(vehicleListUseCaseMock.execute(any(), any())).then { invocation ->
            val observer = invocation.arguments[0] as DisposableObserver<List<Vehicle>>
            observer.onNext(vehicleList)
            observer.onComplete()
        }

        // WHEN
        vehicleListPresenter.onFavCitySelected(1)

        // THEN
        inOrder(viewMock) {
            verify(viewMock).hideRetry()
            verify(viewMock).showLoading()
            verify(viewMock).renderVehiclesOnList(vehicleList)
            verify(viewMock).renderVehiclesOnMap(vehicleList)
            verify(viewMock).hideLoading()
            verify(viewMock).focusMapOn(VehicleListPresenter.HAMBURG)
        }
        verifyNoMoreInteractions(viewMock)
    }

    @Test
    fun whenRetryPressed_thenLoadVehicles(){
        // GIVEN
        given(interactorMock.vehicleListUseCase).willReturn(vehicleListUseCaseMock)
        val vehicleList = ArrayList<Vehicle>()
        vehicleList.add(Vehicle(
                "id", Coordinate(0.0, 0.0), FleetType.POOLING
        ))
        whenever(vehicleListUseCaseMock.execute(any(), any())).then { invocation ->
            val observer = invocation.arguments[0] as DisposableObserver<List<Vehicle>>
            observer.onNext(vehicleList)
            observer.onComplete()
        }

        // WHEN
        vehicleListPresenter.onRetryPressed()

        // THEN
        inOrder(viewMock) {
            verify(viewMock).hideRetry()
            verify(viewMock).showLoading()
            verify(viewMock).renderVehiclesOnList(vehicleList)
            verify(viewMock).renderVehiclesOnMap(vehicleList)
            verify(viewMock).hideLoading()
        }
        verifyNoMoreInteractions(viewMock)
    }
}