package com.marp.mytaxi.pois.base

import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.marp.mytaxi.pois.injector.HasComponent
import javax.inject.Inject

abstract class BaseFragment<P: BasePresenter<*,*>>  : Fragment() {
    @Inject
    protected lateinit var presenter: P

    protected abstract fun injectDependencies()

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        injectDependencies()
    }

    protected fun showToast(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    protected fun <C> getComponent(componentType: Class<C>): C {
        return componentType.cast((activity as HasComponent<C>).component)
    }

    open fun onBackPressed(): Boolean {
        return false
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.addObservers(lifecycle)
    }
}
