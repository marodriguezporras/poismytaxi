package com.marp.mytaxi.pois.injector

interface HasComponent<C> {
    val component: C
}
