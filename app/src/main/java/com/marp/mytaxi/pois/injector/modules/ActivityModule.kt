package com.marp.mytaxi.pois.injector.modules

import android.app.Activity
import com.marp.mytaxi.pois.injector.PerActivity
import dagger.Module
import dagger.Provides

@Module
class ActivityModule(private val activity: Activity) {

    @Provides
    @PerActivity
    internal fun activity(): Activity {
        return this.activity
    }
}
