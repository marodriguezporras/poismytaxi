package com.marp.mytaxi.pois.base

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

open class BasePresenter<I,  V: BasePresenter.View>(private val interactor: I): LifecycleObserver {
    protected lateinit var view: V

    fun attachView(view: V){
        this.view = view
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    open fun resume() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    open fun pause() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    open fun destroy() {
    }

    fun addObservers(lifecycle: Lifecycle) {
        lifecycle.addObserver(this)
    }

    interface View{
        fun showError(message: String?)
        fun hideRetry()
        fun showRetry()
        fun showLoading()
        fun hideLoading()
    }
}