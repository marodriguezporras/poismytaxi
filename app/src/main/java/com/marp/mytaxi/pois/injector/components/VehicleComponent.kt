package com.marp.mytaxi.pois.injector.components

import com.marp.mytaxi.pois.injector.PerActivity
import com.marp.mytaxi.pois.injector.modules.ActivityModule
import com.marp.mytaxi.pois.vehicle.VehicleListFragment
import dagger.Component

@PerActivity
@Component(dependencies = [AppComponent::class], modules = [ActivityModule::class])
interface VehicleComponent : ActivityComponent {
    fun inject(vehicleListFragment: VehicleListFragment)
}
