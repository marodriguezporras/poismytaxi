package com.marp.mytaxi.pois.vehicle.presenter

import com.marp.mytaxi.pois.domain.vehicle.list.VehicleListUseCase
import javax.inject.Inject


class VehicleListInteractor @Inject constructor(val vehicleListUseCase: VehicleListUseCase)