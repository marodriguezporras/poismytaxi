package com.marp.mytaxi.pois.injector.modules

import android.annotation.SuppressLint
import android.content.Context
import com.marp.mytaxi.pois.PoisApplication
import com.marp.mytaxi.pois.data.vehicle.VehicleDataRepository
import com.marp.mytaxi.pois.domain.vehicle.VehicleRepository
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import javax.inject.Named
import javax.inject.Singleton

@Module
class AppModule(private val application: PoisApplication) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context {
        return this.application
    }

    @Provides
    @Singleton
    @Named("worker_thread")
    fun provideExecutorThread(): Scheduler {
        return Schedulers.newThread()
    }

    @Provides
    @Singleton
    @Named("ui_thread")
    fun provideUiThread(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    @Provides
    @Singleton
    fun provideVehicleRepository(vehicleDataRepository: VehicleDataRepository): VehicleRepository {
        return vehicleDataRepository
    }

    @SuppressLint("SimpleDateFormat")
    @Provides
    @Singleton
    fun provideSimpleDateParser(): SimpleDateFormat {
        return SimpleDateFormat("dd/MM/yyyy")
    }
}