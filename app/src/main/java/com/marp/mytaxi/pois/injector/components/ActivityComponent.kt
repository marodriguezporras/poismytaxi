package com.marp.mytaxi.pois.injector.components

import android.app.Activity
import com.marp.mytaxi.pois.injector.PerActivity
import com.marp.mytaxi.pois.injector.modules.ActivityModule
import dagger.Component

@PerActivity
@Component(dependencies = [AppComponent::class], modules = [ActivityModule::class])
interface ActivityComponent {
    fun activity(): Activity
}
