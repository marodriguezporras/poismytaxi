package com.marp.mytaxi.pois

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.marp.mytaxi.pois.injector.components.AppComponent
import com.marp.mytaxi.pois.injector.components.DaggerAppComponent
import com.marp.mytaxi.pois.injector.modules.AppModule
import com.squareup.leakcanary.LeakCanary
import java.util.HashMap

class PoisApplication : MultiDexApplication() {

    val appComponent: AppComponent
        get() {
            val componentType = AppComponent::class.java
            return componentType.cast(components[componentType])!!
        }

    override fun onCreate() {
        super.onCreate()
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return
        }
        this.initializeInjector()
        this.initializeLeakDetection()
    }

    private fun initializeInjector() {
        this.components[AppComponent::class.java] = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
    }

    private fun initializeLeakDetection() {
        if (BuildConfig.DEBUG) {
            LeakCanary.install(this)
        }
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    private val components = HashMap<Class<*>, Any?>()
    fun <C> setComponent(c: Class<C>, component: C) {
        if (BuildConfig.DEBUG)
            this.components[c] = component
    }
}

