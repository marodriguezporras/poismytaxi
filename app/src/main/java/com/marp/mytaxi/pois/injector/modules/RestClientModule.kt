package com.marp.mytaxi.pois.injector.modules

import com.marp.mytaxi.pois.data.net.RestClient
import com.marp.mytaxi.pois.data.vehicle.datasource.cloud.VehicleEndpoints
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
class RestClientModule {
    @Provides
    @Named("base_url")
    fun provideBaseUrl(): String {
        return VehicleEndpoints.API_BASE_URL
    }

    @Provides
    @Singleton
    fun provideRestClient(@Named("base_url") baseUrl: String): RestClient {
        return RestClient(baseUrl)
    }
}
