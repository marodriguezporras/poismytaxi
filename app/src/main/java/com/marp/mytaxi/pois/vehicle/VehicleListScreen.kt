package com.marp.mytaxi.pois.vehicle

import android.content.Context
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.RelativeLayout
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.marp.mytaxi.pois.R
import com.marp.mytaxi.pois.domain.vehicle.models.FleetType
import com.marp.mytaxi.pois.domain.vehicle.models.Vehicle
import com.marp.mytaxi.pois.vehicle.VehicleListFragment.Companion.getLatLngFromCoordinate
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.vehicle_list_fragment.view.*
import kotlinx.android.synthetic.main.view_progress.view.*
import kotlinx.android.synthetic.main.view_retry.view.*
import java.util.HashMap


class VehicleListScreen(context: Context?, private val listener: Listener) :
        RelativeLayout(context),
        OnMapReadyCallback,
        GoogleMap.OnCameraIdleListener,
        GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMapLoadedCallback{


    interface Listener: VehiclesAdapter.OnItemClickListener {
        fun onRetryPressed()
        fun onFavCitySelected(pos: Int)
        fun onMapReady()
        fun onCameraIdle()
        fun onMarkerClick(): Boolean
    }

    private lateinit var vehiclesAdapter: VehiclesAdapter
    private var googleMap: GoogleMap? = null
    private var markers: MutableMap<String, Marker> = HashMap()
    var visibleRegion: LatLngBounds? = null
        get() = googleMap?.projection?.visibleRegion?.latLngBounds
        private set

    init {
        val layout = R.layout.vehicle_list_fragment
        inflate(context, layout, this)
        setupRecyclerView()
        bt_retry.setOnClickListener { listener.onRetryPressed() }
    }

    private fun setupRecyclerView() {
        vehiclesAdapter = VehiclesAdapter(context)
        vehiclesAdapter.onItemClickListener = listener
        rv_vehicles.layoutManager = LinearLayoutManager(context)
        rv_vehicles.adapter = vehiclesAdapter
    }

    fun initMap(mapFragment: SupportMapFragment){
        mapFragment.getMapAsync(this)
    }

    fun renderVehiclesOnList(vehicles: Collection<Vehicle>) {
        this.vehiclesAdapter.setVehiclesList(vehicles)
    }

    fun renderVehiclesOnMap(vehicles: Collection<Vehicle>) {
        googleMap?.clear()
        markers.clear()
        for (vehicle in vehicles)
            addMarker(vehicle)
    }

    private fun addMarker(vehicle: Vehicle) {
        googleMap?.let {
            markers[vehicle.id] = it.addMarker(MarkerOptions()
                    .icon(BitmapDescriptorFactory.fromResource(
                            if (vehicle.fleetType== FleetType.TAXI)
                                R.drawable.taxi_marker
                            else
                                R.drawable.pooling_marker ))
                    .position(getLatLngFromCoordinate(vehicle.coordinate))
                    .title(String.format("%s - %s", vehicle.id, vehicle.fleetType)))
        }
    }

    fun showMarkerVehicleInfo(vehicle: Vehicle) {
        markers[vehicle.id]?.showInfoWindow()
    }

    fun initFavouriteCities(cities: List<String>){
        context?.let {
            val adapter = ArrayAdapter<String>(it,
                    R.layout.spinner_item, android.R.id.text1, cities)
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            fav_cities.adapter = adapter
            fav_cities.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {}
                override fun onItemSelected(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
                    listener.onFavCitySelected(pos)
                }
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap?.let {
            this@VehicleListScreen.googleMap = it
            it.setOnCameraIdleListener(this)
            it.setOnMarkerClickListener(this)
            it.setOnMapLoadedCallback(this)
        }
    }

    override fun onMapLoaded() = listener.onMapReady()

    fun focusMapOn(latLng: LatLng, zoom: Float) {
        googleMap?.let {
            val center = CameraUpdateFactory.newLatLngZoom(latLng, zoom)
            it.animateCamera(center)
        }
    }

    fun focusMapOn(latLngBounds: LatLngBounds) {
        googleMap?.let {
            val center = CameraUpdateFactory.newLatLngBounds(latLngBounds, 0)
            it.animateCamera(center)
        }
    }

    override fun onCameraIdle() = listener.onCameraIdle()

    override fun onMarkerClick(marker: Marker?): Boolean = listener.onMarkerClick()

    fun isPanelOpen(): Boolean = sliding_layout.panelState== SlidingUpPanelLayout.PanelState.EXPANDED

    fun closePanel() {
        sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    fun showLoading() {
        this.rl_progress.visibility = View.VISIBLE
        this.tv_title.visibility = View.GONE
        this.rv_vehicles.visibility = View.GONE
    }

    fun hideLoading() {
        this.rv_vehicles.visibility = View.VISIBLE
        this.tv_title.visibility = View.VISIBLE
        this.rl_progress.visibility = View.GONE
    }

    fun showRetry() {
        this.rl_retry.visibility = View.VISIBLE
        this.tv_title.visibility = View.GONE
    }

    fun hideRetry() {
        this.rl_retry.visibility = View.GONE
    }

    fun resetFavouriteCities() {
        fav_cities.setSelection(0)
    }
}