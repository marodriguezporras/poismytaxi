package com.marp.mytaxi.pois.vehicle

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.marp.mytaxi.pois.R
import com.marp.mytaxi.pois.base.BaseFragment
import com.marp.mytaxi.pois.domain.map.models.Area
import com.marp.mytaxi.pois.domain.map.models.Coordinate
import com.marp.mytaxi.pois.domain.vehicle.models.Vehicle
import com.marp.mytaxi.pois.injector.components.VehicleComponent
import com.marp.mytaxi.pois.vehicle.presenter.VehicleListPresenter


class VehicleListFragment :
        BaseFragment<VehicleListPresenter>(),
        VehicleListPresenter.View,
        VehicleListScreen.Listener {

    private lateinit var vehicleListScreen: VehicleListScreen
    
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                     savedInstanceState: Bundle?): View {
        presenter.attachView(this)
        return getRootView()
    }

    private fun getRootView(): View {
        vehicleListScreen = VehicleListScreen(context, this)
        return vehicleListScreen
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vehicleListScreen.initMap(fragmentManager!!.findFragmentById(R.id.map_fragment) as SupportMapFragment)
        if (savedInstanceState==null)
            presenter.initialize()
    }

    override fun injectDependencies() = getComponent(VehicleComponent::class.java).inject(this)

    override fun onBackPressed(): Boolean {
        if (vehicleListScreen.isPanelOpen()) {
            vehicleListScreen.closePanel()
            return true
        }
        return false
    }

    override fun getVisibleArea(): Area? = getAreaFromLatLngBounds(vehicleListScreen.visibleRegion)

    override fun onMarkerClick() = presenter.onMarkerClick()

    override fun onCameraIdle() = presenter.onCameraIdle()

    override fun onMapReady() = presenter.onMapReady()

    override fun renderVehiclesOnList(vehicles: Collection<Vehicle>) =
            vehicleListScreen.renderVehiclesOnList(vehicles)

    override fun renderVehiclesOnMap(vehicles: Collection<Vehicle>) =
            vehicleListScreen.renderVehiclesOnMap(vehicles)

    override fun initFavouriteCities(cities: List<String>) =
            vehicleListScreen.initFavouriteCities(cities)

    override fun focusMapOn(coordinate: Coordinate, zoom: Float) =
            vehicleListScreen.focusMapOn(getLatLngFromCoordinate(coordinate), zoom)

    override fun focusMapOn(area: Area) =
            vehicleListScreen.focusMapOn(getLatLngBoundsFromArea(area))


    override fun showMarkerVehicleInfo(vehicle: Vehicle) =
            vehicleListScreen.showMarkerVehicleInfo(vehicle)

    override fun resetFavouriteCities() {
        vehicleListScreen.resetFavouriteCities()
    }

    override fun onRetryPressed() = presenter.onRetryPressed()

    override fun onFavCitySelected(pos: Int) = presenter.onFavCitySelected(pos)

    override fun onVehicleItemClicked(vehicle: Vehicle) = presenter.onVehicleClicked(vehicle)

    override fun closePanel() = vehicleListScreen.closePanel()

    override fun showLoading() = vehicleListScreen.showLoading()

    override fun hideLoading() = vehicleListScreen.hideLoading()

    override fun showRetry() = vehicleListScreen.showRetry()

    override fun hideRetry() = vehicleListScreen.hideRetry()

    override fun showError(message: String?) {
        message?.let { this.showToast(it) }
    }

    private fun getLatLngBoundsFromArea(area: Area): LatLngBounds {
        val southwest = LatLng(area.southeast.lat, area.northwest.lon)
        val northeast = LatLng(area.northwest.lat, area.southeast.lon)
        return LatLngBounds(southwest, northeast)
    }

    private fun getAreaFromLatLngBounds(lngBounds: LatLngBounds?): Area?{
        return lngBounds?.let {
            val southwest = lngBounds.southwest
            val northeast = lngBounds.northeast
            return Area(
                    Coordinate(southwest.latitude, northeast.longitude),
                    Coordinate(northeast.latitude, southwest.longitude))
        }
    }

    companion object {
        fun getLatLngFromCoordinate(coordinate: Coordinate): LatLng {
            return LatLng(coordinate.lat, coordinate.lon)
        }
    }
}
