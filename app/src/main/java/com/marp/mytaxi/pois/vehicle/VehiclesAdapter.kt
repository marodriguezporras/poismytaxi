package com.marp.mytaxi.pois.vehicle

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marp.mytaxi.pois.R
import com.marp.mytaxi.pois.domain.vehicle.models.FleetType
import com.marp.mytaxi.pois.domain.vehicle.models.Vehicle
import javax.inject.Inject
import kotlinx.android.synthetic.main.row_vehicle.view.*

class VehiclesAdapter
constructor(context: Context) : RecyclerView.Adapter<VehiclesAdapter.VehicleViewHolder>() {

    private var vehicleList: List<Vehicle> = emptyList()
    var onItemClickListener: OnItemClickListener? = null
    private val layoutInflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehicleViewHolder {
        return VehicleViewHolder(
                this.layoutInflater.inflate(R.layout.row_vehicle, parent, false))
    }

    override fun onBindViewHolder(holder: VehicleViewHolder, position: Int) {
        holder.bindVehicle(vehicleList[position])
    }

    override fun getItemCount(): Int {
        return this.vehicleList.size
    }

    fun setVehiclesList(vehicleCollection: Collection<Vehicle>) {
        this.vehicleList = vehicleCollection as List<Vehicle>
        this.notifyDataSetChanged()
    }

    inner class VehicleViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindVehicle(vehicle: Vehicle){
            itemView.vehicle_id.text = vehicle.id
            itemView.icon_type.setImageResource(
                        if (vehicle.fleetType==FleetType.TAXI)
                            R.drawable.taxi_marker
                        else
                            R.drawable.pooling_marker)
            itemView.fleet_type.text = vehicle.fleetType.toString()
            itemView.fleet_type.setTextColor(Color.parseColor(vehicle.fleetType.color))
            itemView.setOnClickListener {
                onItemClickListener?.onVehicleItemClicked(vehicle)
            }
        }
    }

    interface OnItemClickListener {
        fun onVehicleItemClicked(vehicle: Vehicle)
    }
}
