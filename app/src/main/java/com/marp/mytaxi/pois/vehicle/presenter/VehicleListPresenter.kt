package com.marp.mytaxi.pois.vehicle.presenter

import com.marp.mytaxi.pois.R
import com.marp.mytaxi.pois.base.BasePresenter
import com.marp.mytaxi.pois.domain.map.models.Area
import com.marp.mytaxi.pois.domain.map.models.Coordinate
import com.marp.mytaxi.pois.domain.vehicle.list.VehicleListUseCase
import com.marp.mytaxi.pois.domain.vehicle.models.Vehicle
import com.marp.mytaxi.pois.injector.PerActivity
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

@PerActivity
class VehicleListPresenter
@Inject
constructor(private val vehicleListInteractor: VehicleListInteractor) :
        BasePresenter<VehicleListInteractor, VehicleListPresenter.View>(vehicleListInteractor) {

    private var isLoading = false
    private var vehiclesReloadAllowed = true
    private val favouriteCities = arrayListOf(HAMBURG)

    interface View : BasePresenter.View{
        fun focusMapOn(area: Area)
        fun focusMapOn(coordinate: Coordinate, zoom: Float)
        fun showMarkerVehicleInfo(vehicle: Vehicle)
        fun closePanel()
        fun initFavouriteCities(cities: List<String>)
        fun renderVehiclesOnMap(vehicles: Collection<Vehicle>)
        fun renderVehiclesOnList(vehicles: Collection<Vehicle>)
        fun getVisibleArea(): Area?
        fun resetFavouriteCities()
        fun getString(stringRes: Int): String
    }

    fun onVehicleClicked(vehicle: Vehicle) {
        this.vehiclesReloadAllowed = false
        view.focusMapOn(vehicle.coordinate, 13f)
        view.showMarkerVehicleInfo(vehicle)
        view.closePanel()
    }

    override fun destroy() {
        this.vehicleListInteractor.vehicleListUseCase.unsubscribe()
    }

    private fun showErrorMessage(exception: Exception) {
        val errorMessage = exception.message
        view.showError(errorMessage)
    }

    fun initialize() {
        loadVehicles(HAMBURG)
        val favouriteCitiesString = arrayListOf(view.getString(R.string.select_one))
        favouriteCitiesString.addAll(favouriteCities.map { it.name })
        view.initFavouriteCities(favouriteCitiesString)
    }

    private fun loadVehicles(area: Area) {
        if (this.isLoading)
            return
        this.isLoading = true

        if (!favouriteCities.contains(area))
            view.resetFavouriteCities()

        view.hideRetry()
        view.showLoading()
        this.vehicleListInteractor.vehicleListUseCase.execute(
                VehicleListObserver(),
                VehicleListUseCase.Params.forVehicleList(area.northwest, area.southeast))
    }

    fun onCameraIdle() {
        if (vehiclesReloadAllowed)
            view.getVisibleArea()?.let { this.loadVehicles(it) }
        else
            vehiclesReloadAllowed = true
    }

    fun onMapReady() {
        view.focusMapOn(HAMBURG)
    }

    fun onMarkerClick(): Boolean {
        vehiclesReloadAllowed = false
        return false
    }

    private inner class VehicleListObserver : DisposableObserver<List<Vehicle>>() {
        override fun onNext(vehicles: List<Vehicle>) {
            view.renderVehiclesOnList(vehicles)
            view.renderVehiclesOnMap(vehicles)
        }

        override fun onComplete() {
            view.hideLoading()
            isLoading = false
        }

        override fun onError(e: Throwable) {
            view.hideLoading()
            showErrorMessage(e as Exception)
            view.showRetry()
            isLoading = false
        }
    }

    fun onFavCitySelected(pos: Int) {
        if (pos > 0) {
            vehiclesReloadAllowed = false
            favouriteCities[pos - 1].let {
                loadVehicles(it)
                view.focusMapOn(it)
            }
        }
    }

    fun onRetryPressed() {
        loadVehicles(HAMBURG)
    }

    companion object {
        internal val HAMBURG = Area(
                "Hamburg",
                Coordinate(53.694865, 9.757589),
                Coordinate(53.394655, 10.099891))
    }
}
