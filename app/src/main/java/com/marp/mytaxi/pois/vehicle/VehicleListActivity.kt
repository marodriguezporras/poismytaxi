package com.marp.mytaxi.pois.vehicle

import com.marp.mytaxi.pois.base.BaseActivity
import com.marp.mytaxi.pois.injector.HasComponent
import com.marp.mytaxi.pois.injector.components.DaggerVehicleComponent
import com.marp.mytaxi.pois.injector.components.VehicleComponent


class VehicleListActivity : BaseActivity<VehicleListFragment>(), HasComponent<VehicleComponent> {

    override fun getFragment(): Class<VehicleListFragment> {
        return VehicleListFragment::class.java
    }

    override val component: VehicleComponent
        get() = DaggerVehicleComponent.builder()
                .appComponent(applicationComponent)
                .activityModule(activityModule)
                .build()
}
