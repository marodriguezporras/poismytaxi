package com.marp.mytaxi.pois.injector.components

import android.content.Context
import com.marp.mytaxi.pois.domain.vehicle.VehicleRepository
import com.marp.mytaxi.pois.injector.modules.AppModule
import com.marp.mytaxi.pois.injector.modules.RestClientModule
import dagger.Component
import io.reactivex.Scheduler
import javax.inject.Named
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, RestClientModule::class])
interface AppComponent {

    //Exposed to sub-graphs.
    fun context(): Context

    @Named("ui_thread")
    fun uiThread(): Scheduler

    @Named("worker_thread")
    fun executorThread(): Scheduler

    fun vehicleRepository(): VehicleRepository
}
