package com.marp.mytaxi.pois.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.marp.mytaxi.pois.PoisApplication
import com.marp.mytaxi.pois.R
import com.marp.mytaxi.pois.injector.components.AppComponent
import com.marp.mytaxi.pois.injector.modules.ActivityModule

abstract class BaseActivity<F: BaseFragment<*>> : AppCompatActivity() {

    protected lateinit var currentFragment: F

    abstract fun getFragment(): Class<F>

    protected val applicationComponent: AppComponent
        get() = (application as PoisApplication).appComponent

    protected val activityModule: ActivityModule
        get() = ActivityModule(this)

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fragment)
        initFragment()
    }

    @Suppress("UNCHECKED_CAST")
    private fun initFragment() {
        this.currentFragment = supportFragmentManager
                .fragmentFactory
                .instantiate(classLoader, getFragment().name, null) as F

        currentFragment.arguments = intent.extras
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragment_container, currentFragment, getFragment().name)
        transaction.commit()
    }

    protected fun addFragment(containerViewId: Int, fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(containerViewId, fragment)
        fragmentTransaction.commit()
    }

    override fun onBackPressed() {
        if (currentFragment.onBackPressed()) {
            return
        }
        super.onBackPressed()
    }
}
